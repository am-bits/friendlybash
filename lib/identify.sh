# Copyright 2016 Andreea Mocanu & Alexandru Mocanu
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ----------------------------------------------------------------------
# Authors:
#   andreea.oana.mocanu@gmail.com
#   alexandru.mocanu@gmail.com
#
# Sources:
#   https://bitbucket.org/am-bits/friendlybash

[[ -z "${_identify_sh_included}" ]] && _identify_sh_included='yes' || return

# Usage: identify::here
#
# Prints the current location: '<file_name>:<line_number>'.
function identify::here() {
    echo "${BASH_SOURCE[1]}:${BASH_LINENO[0]}"
}

# Usage: identify::this_function
#
# Prints the name of the current function.
function identify::this_function() {
    echo "${FUNCNAME[1]}"
}

# Usage: identify::caller_function
#
# Prints the name of the calling function, or '<none>' if there is no calling function because our location is main.
function identify::caller_function() {
    if [[ -z ${FUNCNAME[2]} ]]; then
        echo '<none>'
    else
        echo "${FUNCNAME[2]}"
    fi
}

# Usage identify::this_script
#
# Prints the name of the currently executing script
function identify::this_script() {
    local readonly _process_name=$(ps -p "$$" -o comm=)
    if [[ "${_process_name}" = 'bash' ]]; then
        echo "${_process_name}"
    elif [[ -n "${BASH_SOURCE[-1]}" ]]; then
        basename "${BASH_SOURCE[-1]}"
    else
        echo '<unknown>'
    fi
}

# Usage: identify::call
#
# Prints information about the call to the current function:
#   - if the current function is main, the name of the process that started the current script will be printed;
#   - if we are in another function, '<calling_file_name>:<line_number>' will be printed.
function identify::call() {
    if [[ -z ${BASH_SOURCE[2]} ]]; then
        ps -o comm= ${PPID}
    else
        echo "${BASH_SOURCE[2]}:${BASH_LINENO[1]}"
    fi
}

# Usage: identify::call_context
#
# Prints information about the call to the current function:
#   - if the current function is main: '<none> at <calling_process_name>'
#   - if we are in another function: '<caller_function> at <caller_file_name>:<call_line_number>'
function identify::call_context() {
    local _caller_function
    if [[ -z ${FUNCNAME[2]} ]]; then
        _caller_function='<none>'
    else
        _caller_function="${FUNCNAME[2]}"
    fi

    local _call
    if [[ -z ${BASH_SOURCE[2]} ]]; then
        _call=$(ps -o comm= ${PPID})
    else
        _call="${BASH_SOURCE[2]}:${BASH_LINENO[1]}"
    fi

    echo "${_caller_function} at ${_call}"
}

# Usage: identify::todo
#
# Prints a TODO message with the current location: '<file_name>:<line_number>'.
function identify::todo() {
    echo -ne "\e[33m"
    echo -n "TODO ${BASH_SOURCE[1]}:${BASH_LINENO[0]}"
    echo -e "\e[0m"
}

