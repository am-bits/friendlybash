# Copyright 2016 Andreea Mocanu & Alexandru Mocanu
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ----------------------------------------------------------------------
# Authors:
#   andreea.oana.mocanu@gmail.com
#   alexandru.mocanu@gmail.com
#
# Sources:
#   https://bitbucket.org/am-bits/friendlybash

[[ -z "${_text_sh_included}" ]] && _text_sh_included='yes' || return

# Usage: text::lower <string>
#
# Converts the given string to lower case.
#
#   string  a string
function text::lower() {
    echo -n "$*" | tr '[:upper:]' '[:lower:]'
}

# Usage: text::upper <string>
#
# Converts the given string to upper case.
#
#   string  a string
function text::upper() {
    echo -n "$*" | tr '[:lower:]' '[:upper:]'
}

# Usage: text::remove_format_sequences <string>
#
# Removes all format sequences (e.g. \e[31m, \e[38;5;199m) from the given string
#
#   string  a string
function text::remove_format_sequences() {
    echo -n "$*" | sed -r 's/\x1b\[[0-9;]*m?//g'
}

# Usage: text::remove_control_characters <string>
#
# Removes all ASCII control characters (except LF and TAB!) from the given string
#
#   string  a string
function text::remove_control_characters() {
    echo -n "$*" | sed -r 's/[\x01-\x08\x0B-\x1F\x7F]//g'
}

# Usage: text::sanitize <string>
#
# Replaces all ASCII control characters in the given string (except LF and TAB!) 
# with a textual representation of their caret notation.
#
#   string  a string
function text::sanitize() {
    echo -n "$*" \
    | sed -r "s/${CODE_NUL}/^@/g" \
    | sed -r "s/${CODE_SOH}/^A/g" \
    | sed -r "s/${CODE_STX}/^B/g" \
    | sed -r "s/${CODE_ETX}/^C/g" \
    | sed -r "s/${CODE_EOT}/^D/g" \
    | sed -r "s/${CODE_ENQ}/^E/g" \
    | sed -r "s/${CODE_ACK}/^F/g" \
    | sed -r "s/${CODE_BEL}/^G/g" \
    | sed -r "s/${CODE_BS}/^H/g" \
    | sed -r "s/${CODE_VT}/^K/g" \
    | sed -r "s/${CODE_FF}/^L/g" \
    | sed -r "s/${CODE_CR}/^M/g" \
    | sed -r "s/${CODE_SO}/^N/g" \
    | sed -r "s/${CODE_SI}/^O/g" \
    | sed -r "s/${CODE_DLE}/^P/g" \
    | sed -r "s/${CODE_DC1}/^Q/g" \
    | sed -r "s/${CODE_DC2}/^R/g" \
    | sed -r "s/${CODE_DC3}/^S/g" \
    | sed -r "s/${CODE_DC4}/^T/g" \
    | sed -r "s/${CODE_NAK}/^U/g" \
    | sed -r "s/${CODE_SYN}/^V/g" \
    | sed -r "s/${CODE_ETB}/^W/g" \
    | sed -r "s/${CODE_CAN}/^X/g" \
    | sed -r "s/${CODE_EM}/^Y/g" \
    | sed -r "s/${CODE_SUB}/^Z/g" \
    | sed -r "s/${CODE_ESC}/^[/g" \
    | sed -r "s/${CODE_FS}/^\\\\/g" \
    | sed -r "s/${CODE_GS}/^]/g" \
    | sed -r "s/${CODE_RS}/^^/g" \
    | sed -r "s/${CODE_US}/^_/g" \
    | sed -r "s/${CODE_DEL}/^?/g"
}

# constants for control codes
readonly CODE_NUL='\x00'  # Null
readonly CODE_SOH='\x01'  # Start of Heading
readonly CODE_STX='\x02'  # Start of Text
readonly CODE_ETX='\x03'  # End of Text
readonly CODE_EOT='\x04'  # End of Transmission
readonly CODE_ENQ='\x05'  # Enquiry
readonly CODE_ACK='\x06'  # Acknowledgment
readonly CODE_BEL='\x07'  # Bell
readonly CODE_BS='\x08'   # Backspace
readonly CODE_HT='\x09'   # Horizontal Tab
readonly CODE_LF='\x0A'   # Line Feed
readonly CODE_VT='\x0B'   # Vertical Tab
readonly CODE_FF='\x0C'   # Form Feed
readonly CODE_CR='\x0D'   # Carriage Return
readonly CODE_SO='\x0E'   # Shift Out
readonly CODE_SI='\x0F'   # Shift In
readonly CODE_DLE='\x10'  # Data Link Escape
readonly CODE_DC1='\x11'  # Device Control 1 (oft. XON)
readonly CODE_DC2='\x12'  # Device Control 2
readonly CODE_DC3='\x13'  # Device Control 3 (oft. XOFF)
readonly CODE_DC4='\x14'  # Device Control 4
readonly CODE_NAK='\x15'  # Negative Acknowledgment
readonly CODE_SYN='\x16'  # Synchronous Idle
readonly CODE_ETB='\x17'  # End of Transmission Block
readonly CODE_CAN='\x18'  # Cancel
readonly CODE_EM='\x19'   # End of Medium
readonly CODE_SUB='\x1A'  # Substitute
readonly CODE_ESC='\x1B'  # Escape
readonly CODE_FS='\x1C'   # File Separator
readonly CODE_GS='\x1D'   # Group Separator
readonly CODE_RS='\x1E'   # Record Separator
readonly CODE_US='\x1F'   # Unit Separator
readonly CODE_DEL='\x7F'  # Delete


# Usage: text::untabify <string>
#
# Replaces all TABs in the given string with spaces.
# The number of spaces per tab is specified by the global variable TEXT__SPACES_PER_TAB (default value: 4).
#
#   string          a string
TEXT__SPACES_PER_TAB=4
function text::untabify() {
    local readonly _spaces=$(printf "%${TEXT__SPACES_PER_TAB}s" ' ')
    echo -n "$*" | sed -r "s/${CODE_HT}/${_spaces}/g"
}

# Usage: text::sanitize_and_untabify <string>
#
# Sanitizes the given string and replaces all TABs with spaces.
#
#   string  a string
function text::sanitize_and_untabify() {
    text::untabify "$(text::sanitize "$*")"
}

# Usage: text::trim_whitespace <string>
#
# Removes leading and trailing whitespaces from the given string.
#
#   string  a string
function text::trim_whitespace() {
    echo -ne "$*" | sed -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//'
}

