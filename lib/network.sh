# Copyright 2016 Andreea Mocanu & Alexandru Mocanu
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ----------------------------------------------------------------------
# Authors:
#   andreea.oana.mocanu@gmail.com
#   alexandru.mocanu@gmail.com
#
# Sources:
#   https://bitbucket.org/am-bits/friendlybash

[[ -z "${_network_sh_included}" ]] && _network_sh_included='yes' || return

source "${FRIENDLY_BASH_ROOT}/lib/util.sh"

function network::test_ping() {
    util::mandatory_arg_count 1 "$@"
    local readonly _address="$1"
    
    ping -q -w 1 -c 1 "${_address}" > /dev/null && return 0 || return 1
}

function network::test_wget() {
    util::mandatory_arg_count 1 "$@"
    local readonly _url="$1"
    
    wget -q --tries=10 --timeout=20 --spider "${_url}" && return 0 || return 1
}

# shellcheck disable=SC2120
function network::default_gateway() {
    util::recommended_arg_count 0 "$@"

    ip r | grep default | cut -d ' ' -f 3
}

function network::test_default_gateway() {
    util::recommended_arg_count 0 "$@"

    # shellcheck disable=SC2119
    network::test_ping "$(network::default_gateway)"
}

function network::test_internet_connection() {
    util::recommended_arg_count 0 "$@"

    local readonly _addresses_to_test=('www.google.com' 'www.yahoo.com' 'www.facebook.com' 'www.twitter.com' 'www.stackoverflow.com')
    local _ping_success='false'
    local _wget_success='false'

    local i
    for i in "${_addresses_to_test[@]}"
    do
        network::test_ping "$i"
        [[ $? -eq 0 ]] && _ping_success='true'
        
        network::test_wget "$i"
        [[ $? -eq 0 ]] && _wget_success='true'
        
        [[ ${_ping_success} = 'true' && ${_wget_success} = 'true' ]] && return 0
    done

    return 1
}
