# Copyright 2016 Andreea Mocanu & Alexandru Mocanu
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ----------------------------------------------------------------------
# Authors:
#   andreea.oana.mocanu@gmail.com
#   alexandru.mocanu@gmail.com
#
# Sources:
#   https://bitbucket.org/am-bits/friendlybash

[[ -z "${_util_sh_included}" ]] && _util_sh_included='yes' || return

source "${FRIENDLY_BASH_ROOT}/lib/identify.sh"
source "${FRIENDLY_BASH_ROOT}/lib/log.sh"

# Usage: util::mandatory_arg_count <expected_argument_count> [<argument_list>]...
#
# Can be called by a script or function to check that it received the expected number of arguments.
# Exits the script if the number of arguments is different than expected.
#
#   expected_argument_count   the expected number of arguments
#   argument_list             the actual list of arguments
function util::mandatory_arg_count() {
	[[ -n $1 ]] || util::fatal_error "$(identify::call_context): $(identify::this_function) requires at least 1 argument"

	local readonly _expected_arg_count="$1"
	local readonly _actual_arg_count="$((${#}-1))"

	if [[ "${_actual_arg_count}" -ne "${_expected_arg_count}" ]]; then
		local _context
        local _checked_entity
        if [[ -z ${BASH_SOURCE[2]} ]]; then
            _context=$(ps -o comm= ${PPID})
            _checked_entity=${BASH_SOURCE[1]}
        else
            _context="${BASH_SOURCE[2]}:${BASH_LINENO[1]}"
            _checked_entity=${FUNCNAME[1]}
        fi
        util::fatal_error "${_context} called ${_checked_entity} with incorrect number of arguments: expected ${_expected_arg_count}, actual ${_actual_arg_count}"
	fi
}

# Usage: util::mandatory_min_arg_count <expected_minimum_argument_count> [<argument_list>]...
#
# Can be called by a script or function to check that it received at least a minimum number of arguments.
# Exits the script if the number of arguments is less than the given minimum.
#
#   expected_minimum_argument_count     the minimum expected number of arguments
#   argument_list                       the actual list of arguments
function util::mandatory_min_arg_count() {
	[[ -n $1 ]] || util::fatal_error "$(identify::call_context): $(identify::this_function) requires at least 1 argument"

	local readonly _expected_min_arg_count="$1"
	local readonly _actual_arg_count="$((${#}-1))"

	if [[ "${_actual_arg_count}" -lt "${_expected_min_arg_count}" ]]; then
		local _context
        local _checked_entity
        if [[ -z ${BASH_SOURCE[2]} ]]; then
            _context=$(ps -o comm= ${PPID})
            _checked_entity=${BASH_SOURCE[1]}
        else
            _context="${BASH_SOURCE[2]}:${BASH_LINENO[1]}"
            _checked_entity=${FUNCNAME[1]}
        fi
        util::fatal_error "${_context} called ${_checked_entity} with incorrect number of arguments: expected at least ${_expected_min_arg_count}, actual ${_actual_arg_count}"
	fi
}

# Usage: util::recommended_arg_count <expected_argument_count> [<argument_list>]...
#
# Can be called by a script or function to check if it received the expected number of arguments.
# Logs a warning if the number of arguments is different than expected, but exits with status 0.
#
#   expected_argument_count   the expected number of arguments
#   argument_list             the actual list of arguments
function util::recommended_arg_count() {
	[[ -n $1 ]] || util::fatal_error "$(identify::call_context): $(identify::this_function) requires at least 1 argument"

	local readonly _expected_arg_count="$1"
	local readonly _actual_arg_count="$((${#}-1))"

	if [[ "${_actual_arg_count}" -ne "${_expected_arg_count}" ]]; then
		local _context
        local _checked_entity
        if [[ -z ${BASH_SOURCE[2]} ]]; then
            _context=$(ps -o comm= ${PPID})
            _checked_entity=${BASH_SOURCE[1]}
        else
            _context="${BASH_SOURCE[2]}:${BASH_LINENO[1]}"
            _checked_entity=${FUNCNAME[1]}
        fi
        log::warning "${_context} called ${_checked_entity} with incorrect number of arguments: expected ${_expected_arg_count}, actual ${_actual_arg_count}"
	    if [[ "${_actual_arg_count}" -gt "${_expected_arg_count}" ]]; then
            if [[ "${_expected_arg_count}" -eq 0 ]]; then
                log::warning 'All passed arguments will be ignored'
            else
                log::warning "Only the first ${_expected_arg_count} argument(s) will be used"
            fi
        fi
    fi
}

# Usage: util::is_absolute_path <path>
#
# Returns 0 if the given path is absolute, and 1 if not.
#
#   path        a path to check
function util::is_absolute_path() {
    util::mandatory_arg_count 1 "$@"
    [[ "${1:0:1}" = "/" ]] && return 0 || return 1
}

# Usage: util::run_script_with_logging <log_file> <script> [<script_arguments>]...
#
# Runs the given script (with the given arguments) and saves the output of log function calls (from log.sh library) to the log_file.
# Requires environment variable LOG_STREAM to be set to a value different than 0 (STDIN), 1 (STDOUT) or 2 (STDERR).
#
#   log_file            the log file (absolute path)
#   script              the script to run (absolute path)
#   script_arguments    arguments for the script
function util::run_script_with_logging() {
    util::mandatory_min_arg_count 2 "$@"
    util::is_absolute_path "$1" || util::fatal_error "Wrong $(identify::this_function) call in $(identify::call): expected absolute path for \$1"
    util::is_absolute_path "$2" || util::fatal_error "Wrong $(identify::this_function) call in $(identify::call): expected absolute path for \$2"
    [[ -x $2 ]] || util::fatal_error "Wrong $(identify::this_function) call in $(identify::call): '$2' missing or not executable"
    [[ -n ${LOG_STREAM} ]] || util::fatal_error 'Logging stream environment variable (LOG_STREAM) not set'
    
    log::debug "Executing script '$2' with arguments [${*:3}]"
    eval "$2 ${*:3} ${LOG_STREAM}>> $1"
    local readonly _exit_status="$?"
    log::debug "Script '$2' exited with status ${_exit_status}"
    return "${_exit_status}"
}

# Usage: util::create_file <file> <access_rights>
#
# Creates the given file with the given access rights.
#
#   file            file to create (absolute or relative path)
#   access_rights   access rights for the new file (chmod format)
function util::create_file() {
    util::mandatory_arg_count 2 "$@"
    local readonly _file="$1"
    local readonly _rights="$2"

    if [[ -a ${_file} ]]; then
        log::error "File already exists: '${_file}'"
        return 1
    fi

    touch "${_file}"
    if [[ $? -ne 0 ]]; then
        log::error "Could not create file '${_file}'"
        return 1
    fi    

    chmod "${_rights}" "${_file}"
    if [[ $? -ne 0 ]]; then
        log::error "Could not change access right for file '${_file}' to '${_rights}'"
        return 1
    fi

    log::debug "File created: '${_file}'"
    return 0
}

# Usage: util::create_dir <directory> <access_rights>
#
# Creates the given directory with the given access rights.
# If the directory already exists, the function is successful only if it is empty.
#
#   directory           directory to create (absolute or relative path)
#   access_rights       access rights for the new directory (chmod format)
function util::create_dir() {
    util::mandatory_arg_count 2 "$@"
    local readonly _dir="$1"
    local rights _rights="$2"

    if [[ -d ${_dir} ]]; then
        if [ "$(ls -A "${_dir}")" ]; then
            log::error "Directory already exists and is not empty: '${_dir}'"
            return 1
        else
            log::warning "Directory already exists and is empty: '${_dir}'"
        fi
    else
        mkdir -p "${_dir}"
        if [[ $? -ne 0 ]]; then
            log::error "Could not create directory: '${_dir}'"
            return 1
        fi
    fi

    chmod "${_rights}" "${_dir}"
    if [[ $? -ne 0 ]]; then
        log::error "Could not change access right for directory '${_dir}' to '${_rights}'"
        return 1
    fi

    log::debug "Directory created: '${_dir}'"
    return 0
}

# Usage: util::program_is_installed <program_name>
#
# Checks whether the given program is installed.
#
#   program_name    name of a program/application
function util::program_is_installed() {
    util::mandatory_arg_count 1 "$@"
    local readonly _program="$1"    

    hash "${_program}" 2> /dev/null
    [[ $? -ne 0 ]] && return 1 || return 0
}

# Usage: util::mandatory_program_is_installed <program_name>
#
# Tests whether the given program is installed and exits the current script if not.
#
#   program_name    name of a program/application
function util::mandatory_program_is_installed() {
    util::mandatory_arg_count 1 "$@"
    local readonly _program="$1"

    util::program_is_installed "${_program}" || util::fatal_error "Mandatory program '${_program}' not installed"
}

# Usage: util::battery_level
#
# Retrieves the current battery level.
function util::battery_level() {
    util::recommended_arg_count 0 "$@"
    util::mandatory_program_is_installed 'acpi'
    
    acpi -b | grep -P -o '[0-9]+(?=%)'
}

# Usage: util::battery_is_charging
#
# Checks whether the battery is charging or not. Fully charged is assimilated to charging.
function util::battery_is_charging() {
    util::recommended_arg_count 0 "$@"
    util::mandatory_program_is_installed 'acpi'
    
    local readonly _charging_status=$(acpi -b | grep -P -o '[a-zA-Z]+(?=,)')
    [[ ${_charging_status} = 'Discharging' ]] && return 1 || return 0
}

# Usage: util::generate_timestamp_id
#
# Returns the current timestamp as a unique ID.
function util::generate_timestamp_id() {
    util::recommended_arg_count 0 "$@"

    date +"%Y-%m-%d_%H-%M-%S"
}

# Usage: util::validate_archive <archive_path>
#
# Checks the integrity of the given tar archive. Exits if archive missing or integrity check fails.
#
#   archive_path    absolute path to the archive
function util::validate_archive() {
    util::mandatory_arg_count 1 "$@"
    local readonly _archive="$1"

    [[ -f "${_archive}" ]] || util::fatal_error "Archive is missing: ${_archive}"

    tar -tf "${_archive}" > /dev/null 2>&1
    [[ $? -eq 0 ]] || util::fatal_error "Archive is corrupted: ${_archive}"
}

# Usage: util::compute_hash <input_string>
#
# Computes a unique hash for the given input string
#
#   input_string    a string
function util::compute_hash() {
    util::recommended_arg_count 1 "$@"
    local readonly _input_string="$1"

    echo "${_input_string}" | md5sum | cut -d ' ' -f 1
}

# Usage: util::totalsize_human_readable <file_or_directory>
#
# Returns the size of the given file or directory with the appropriate unit of size.
#
#   file_or_directory   the absolute or relative path to a file or directory in the filesystem
function util::totalsize_human_readable() {
    util::mandatory_arg_count 1 "$@"
    local readonly _file="$1"    
    [[ -n "${_file}" ]] || util::fatal_error 'No file or directory received'

    du -ch "${_file}" | grep -E ".*\stotal$" | cut -d $'\t' -f 1 2> /dev/null
}

# Usage: util::totalsize <file_or_directory>
#
# Returns the size of the given file or directory in kilobytes.
#
#   file_or_directory   the absolute or relative path to a file or directory in the filesystem
function util::totalsize() {
    util::mandatory_arg_count 1 "$@"
    local readonly _file="$1"    
    [[ -n "${_file}" ]] || util::fatal_error 'No file or directory received'

    du -c "${_file}" | grep -E ".*\stotal$" | cut -d $'\t' -f 1 2> /dev/null
}

# Usage: util::fatal_error <error_message>
#
# Logs the given error message and exits the script.
#
#   error_message   the error message to log
function util::fatal_error() {
    log::error "A fatal error has occured: '$*'"

    local i=60
    local _any_character
    while true; do
        echo "Press any key to exit now... (automatic exit in $i seconds)"
        (( i==0 )) && break
        (( i-- ))

        # shellcheck disable=SC2034
        read  -t 1 -n 1 _any_character
        if [[ $? -eq 0 ]]; then
            # erase the typed character
            if [[ -z "${_any_character}" ]]; then
                tput cuu1 2> /dev/null
            else
                tput cub 1 2> /dev/null
                tput el 2> /dev/null
            fi
            break
        fi
        tput cuu1 2> /dev/null
        tput el 2> /dev/null
    done

    # shellcheck disable=SC2119
    local readonly _pgid=$(util::current_process_group)
    kill -- -"${_pgid}"
}

# Usage: util::process_exists <pid>
#
# Checks whether a process with the given PID exists.
#
#   pid     a process id
function util::process_exists() {
    util::mandatory_arg_count 1 "$@"
    ps -p "$1" > /dev/null 2>&1
}

# Usage: util::curent_process_group
#
# Returns the the process group ID of the current process.
# shellcheck disable=SC2120
function util::current_process_group() {
    util::recommended_arg_count 0 "$@"
    ps -h -p $$ -o "%r" 2> /dev/null | tr -d ' '
}

