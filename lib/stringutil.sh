# Copyright 2016 Andreea Mocanu & Alexandru Mocanu
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ----------------------------------------------------------------------
# Authors:
#   andreea.oana.mocanu@gmail.com
#   alexandru.mocanu@gmail.com
#
# Sources:
#   https://bitbucket.org/am-bits/friendlybash

[[ -z "${_stringutil_sh_included}" ]] && _stringutil_sh_included='yes' || return

source "${FRIENDLY_BASH_ROOT}/lib/identify.sh"
source "${FRIENDLY_BASH_ROOT}/lib/util.sh"

# Usage: stringutil::repeat <string> <times_to_repeat>
#
# Repeats the given string for the given number of times.
#
#   string              a string
#   times_to_repeat     how many times to repeat the string
function stringutil::repeat() {
    util::mandatory_arg_count 2 "$@"
    [[ "$2" -eq '0' || "$2" =~ ^[1-9][0-9]*$ ]] || util::fatal_error "$(identify::call): expected \$2 for $(identify::this_function) to be a number, actual: $2"

    local readonly _string="$1"
    local readonly _times_to_repeat="$2"

    local _result=''
    for (( i=0; i<_times_to_repeat; i++ )); do
        _result="${_result}${_string}"
    done

    echo -n "${_result}"
}

