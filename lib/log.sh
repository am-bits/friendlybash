# Copyright 2016 Andreea Mocanu & Alexandru Mocanu
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ----------------------------------------------------------------------
# Authors:
#   andreea.oana.mocanu@gmail.com
#   alexandru.mocanu@gmail.com
#
# Sources:
#   https://bitbucket.org/am-bits/friendlybash

[[ -z "${_log_sh_included}" ]] && _log_sh_included='yes' || return

source "${FRIENDLY_BASH_ROOT}/lib/text.sh"
source "${FRIENDLY_BASH_ROOT}/lib/identify.sh"

readonly LOG_STREAM=3
readonly ISSUES_LOG_STREAM=4

# Usage: log::can_log_to_file
#
# Returns 0 if the stream corresponding to the LOG_STREAM value is open.
# Returns 1 otherwise.
function log::can_log_to_file() {
    if { >&"${LOG_STREAM}"; } 2> /dev/null; then
        return 0
    else
        return 1
    fi
}

# Usage: log::can_log_to_issues_file
#
# Returns 0 if the stream corresponding to the ISSUES_LOG_STREAM value is open.
# Returns 1 otherwise.
function log::can_log_to_issues_file() {
    if { >&"${ISSUES_LOG_STREAM}"; } 2> /dev/null; then
        return 0
    else
        return 1
    fi
}

# Usage: log::new_section
#
# Marks the beginning of a new log section by printing the caller script name and the timestamp to the STDOUT and to the LOG_STREAM if available.
function log::new_section() {
    local readonly _timestamp=$(date +"%H:%M:%S %d-%m-%Y") 
    local readonly _new_section_tag="[# ${_timestamp} >> $(identify::this_script) #]"
    
    echo -ne "\n\t\e[1;33m" >&1
    echo -n "${_new_section_tag}" >&1
    echo -e "\e[0m\n" >&1
    if log::can_log_to_file; then
        echo -ne "\n\t" >&"${LOG_STREAM}"
        echo -n "${_new_section_tag}" >&"${LOG_STREAM}"
        echo -e "\n" >&"${LOG_STREAM}"
    fi
}

# Usage: log::debug <degub_message>
#
# Prints the given info message to the STDOUT and to the LOG_STREAM if available.
#
#   debug_message    the message to print
function log::debug() {
    local readonly _message="$(text::sanitize_and_untabify "$1")"
	local readonly _context="$(identify::call)"
 
    if [[ $# -ne 1 ]]; then
        local readonly _usage_warning="$(identify::this_function) takes exactly 1 argument, but $# provided"
        echo -ne "\e[1;34m" >&1
        echo -n "WARN  [${_context}]: ${_usage_warning}" >&1
        echo -e "\e[0m" >&1
        if log::can_log_to_file; then
            echo "[WRN][$(date +'%Y-%m-%dT%H:%M:%S.%3N%z')][${_context}]: ${_usage_warning}" >&"${LOG_STREAM}"
        fi
    fi
    
    if log::can_log_to_file; then
        echo "[DBG][$(date +'%Y-%m-%dT%H:%M:%S.%3N%z')][${_context}]: ${_message}" >&"${LOG_STREAM}"
    fi
}

# Usage: log::info <info_message>
#
# Prints the given info message to the STDOUT and to the LOG_STREAM if available.
#
#   info_message    the message to print
function log::info() {
    local readonly _message="$(text::sanitize_and_untabify "$1")"
	local readonly _context="$(identify::call)"
 
    if [[ $# -ne 1 ]]; then
        local readonly _usage_warning="$(identify::this_function) takes exactly 1 argument, but $# provided"
        echo -ne "\e[1;34m" >&1
        echo -n "WARN  [${_context}]: ${_usage_warning}" >&1
        echo -e "\e[0m" >&1
        if log::can_log_to_file; then
            echo "[WRN][$(date +'%Y-%m-%dT%H:%M:%S.%3N%z')][${_context}]: ${_usage_warning}" >&"${LOG_STREAM}"
        fi
    fi
    
    echo -ne "\e[0;32m" >&1
    echo -n "${_message}" >&1
    echo -e "\e[0m" >&1
    if log::can_log_to_file; then
        echo "[INF][$(date +'%Y-%m-%dT%H:%M:%S.%3N%z')][${_context}]: ${_message}" >&"${LOG_STREAM}"
    fi
}

# Usage: log::warning <warning_message>
#
# Prints the given warning message to the STDOUT and to the LOG_STREAM if available.
#
#   warning_message    the message to print
function log::warning() {
    local readonly _message="$(text::sanitize_and_untabify "$1")"
    local readonly _context="$(identify::call)"
     
    if [[ $# -ne 1 ]]; then
        local readonly _usage_warning="$(identify::this_function) takes exactly 1 argument, but $# provided"
        echo -ne "\e[1;34m" >&1
        echo -n "WARN  [${_context}]: ${_usage_warning}" >&1
        echo -e "\e[0m" >&1
        if log::can_log_to_file; then
            echo "[WRN][$(date +'%Y-%m-%dT%H:%M:%S.%3N%z')][${_context}]: ${_usage_warning}" >&"${LOG_STREAM}"
        fi 
    fi
    
    echo -ne "\e[1;34m" >&1
    echo -n "${_message}" >&1
    echo -e "\e[0m" >&1
    if log::can_log_to_file; then
        echo "[WRN][$(date +'%Y-%m-%dT%H:%M:%S.%3N%z')][${_context}]: ${_message}" >&"${LOG_STREAM}"
    fi
    if log::can_log_to_issues_file; then
        echo "[WRN][$(date +'%Y-%m-%dT%H:%M:%S.%3N%z')][${_context}]: ${_message}" >&"${ISSUES_LOG_STREAM}"
    fi
}

# Usage: log::error <error_message>
#
# Prints the given error message to the STDERR and to the LOG_STREAM if available.
#
#   error_message    the message to print
function log::error() {
    local readonly _message="$(text::sanitize_and_untabify "$1")"
    local readonly _context="$(identify::call)"
	  
    if [[ $# -ne 1 ]]; then
        local readonly _usage_warning="$(identify::this_function) takes exactly 1 argument, but $# provided"
        echo -ne "\e[1;34m" >&1
        echo -n "WARN  [${_context}]: ${_usage_warning}" >&1
        echo -e "\e[0m" >&1
        if log::can_log_to_file; then
            echo "[WRN][$(date +'%Y-%m-%dT%H:%M:%S.%3N%z')][${_context}]: ${_usage_warning}" >&"${LOG_STREAM}"
        fi 
    fi
   
    echo -ne "\e[0;31m" >&2
    echo -n "${_message}" >&2
    echo -e "\e[0m" >&2
    if log::can_log_to_file; then
        echo "[ERR][$(date +'%Y-%m-%dT%H:%M:%S.%3N%z')][${_context}]: ${_message}" >&"${LOG_STREAM}"
    fi
    if log::can_log_to_issues_file; then
        echo "[ERR][$(date +'%Y-%m-%dT%H:%M:%S.%3N%z')][${_context}]: ${_message}" >&"${ISSUES_LOG_STREAM}"
    fi
}

# Usage: log::lock_error <lock_error_message>
#
# Prints the given error message to the STDERR and to the LOG_STREAM if available.
# This function should be called only to log a failed lock attempt.
#
#   lock_error_message    the message to print
function log::lock_error() {
    local readonly _message="$(text::sanitize_and_untabify "$1")"
    local readonly _context="$(identify::call)"
	  
    if [[ $# -ne 1 ]]; then
        local readonly _usage_warning="$(identify::this_function) takes exactly 1 argument, but $# provided"
        echo -ne "\e[1;34m" >&1
        echo -n "WARN  [${_context}]: ${_usage_warning}" >&1
        echo -e "\e[0m" >&1
        if log::can_log_to_file; then
            echo "[WRN][$(date +'%Y-%m-%dT%H:%M:%S.%3N%z')][${_context}]: ${_usage_warning}" >&"${LOG_STREAM}"
        fi 
    fi
   
    echo -ne "\e[1;35m" >&2
    echo -n "${_message}" >&2
    echo -e "\e[0m" >&2
    if log::can_log_to_file; then
        echo "[LCK][$(date +'%Y-%m-%dT%H:%M:%S.%3N%z')][${_context}]: ${_message}" >&"${LOG_STREAM}"
    fi
    if log::can_log_to_issues_file; then
        echo "[LCK][$(date +'%Y-%m-%dT%H:%M:%S.%3N%z')][${_context}]: ${_message}" >&"${ISSUES_LOG_STREAM}"
    fi
}

