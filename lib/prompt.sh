# Copyright 2016 Andreea Mocanu & Alexandru Mocanu
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ----------------------------------------------------------------------
# Authors:
#   andreea.oana.mocanu@gmail.com
#   alexandru.mocanu@gmail.com
#
# Sources:
#   https://bitbucket.org/am-bits/friendlybash

[[ -z "${_prompt_sh_included}" ]] && _prompt_sh_included='yes' || return

source "${FRIENDLY_BASH_ROOT}/lib/util.sh"
source "${FRIENDLY_BASH_ROOT}/lib/format.sh"
source "${FRIENDLY_BASH_ROOT}/lib/display.sh"
source "${FRIENDLY_BASH_ROOT}/lib/text.sh"

# this list of attributes controls how the user input is displayed as its being typed
PROMPT__USER_INPUT_ATTRS=("${COLOUR_YELLOW}")
# this list specifies the attributes to revert back to after the accepted input is displayed
PROMPT__POST_USER_INPUT_ATTRS=("${COLOUR_DEFAULT}")

# this list of attributes controls how the user input is displayed after it is accepted
PROMPT__VALID_INPUT_ATTRS=("${COLOUR_WHITE}")
# this list specifies the attributes to revert back to after the accepted input is displayed
PROMPT__POST_VALID_INPUT_ATTRS=("${COLOUR_DEFAULT}")

# this list of attributes controls how the user input is displayed if it is rejected
PROMPT__INVALID_INPUT_ATTRS=("${COLOUR_BLACK}" "${BACKGROUND_RED}")
# this list specifies the attributes to revert back to after the rejected input is displayed
PROMPT__POST_INVALID_INPUT_ATTRS=("${COLOUR_DEFAULT}" "${BACKGROUND_DEFAULT}")

# this list of attributes controls how the shortcuts encoded into choice menus are displayed
PROMPT__SHORTCUT_ATTRS=("${STYLE_UNDERLINED}" "${COLOUR_BLUE}")
# this list specifies the attributes to revert back to after the shortcut is displayed
PROMPT__POST_SHORTCUT_ATTRS=("${STYLE_NOT_UNDERLINED}" "${COLOUR_DEFAULT}")

# setting this to 'true' hides the cursor after reading any input
PROMPT__KEEP_CURSOR_HIDDEN='false'

# Usage: promp::press_to_continue
#
# Waits for user to press any key to continue.
function prompt::press_to_continue() {
    local _initial_user_interaction
    prompt::user_interaction_is_enabled && _initial_user_interaction=0 || _initial_user_interaction=1

    display::show_cursor
    prompt::disable_user_interaction
    _prompt::flush_stdin

    local _char
    read -n 1 -p 'Press any key to continue... ' _char

    _prompt::update_cursor_visibility

    [[ "${_initial_user_interaction}" -eq 0 ]] && prompt::enable_user_interaction
}

# Usage: prompt::input [<message>]
#
# Prompts the user to give input, using the given message.
#
#   message       a message describing the requested input
#   return value  the input
function prompt::input() {
    trap '_prompt::resize' WINCH

    local _initial_user_interaction
    prompt::user_interaction_is_enabled && _initial_user_interaction=0 || _initial_user_interaction=1

    prompt::disable_user_interaction >&2
    display::hide_cursor >&2

    local _message
    [[ -z "$*" ]] && _message='Your input' || _message="$*"
    _message="${_message}: "
    _prompt::display_message "${_message}" >&2

    _prompt::read_input >&2
    local readonly _input="${_PROMPT__CURRENT_INPUT}"

    _prompt::accept_input "$(text::sanitize "${_input}")" >&2

    echo -n "${_input}"

    _prompt::update_cursor_visibility >&2
    [[ "${_initial_user_interaction}" -eq 0 ]] && prompt::enable_user_interaction >&2
}

# Usage: prompt::input [<message>]
#
# Same as prompt::input, but doesn't accept empty input.
#
#   message       a message describing the requested input
#   return value  the input
function prompt::input_not_empty() {
    trap '_prompt::resize' WINCH

    local _initial_user_interaction
    prompt::user_interaction_is_enabled && _initial_user_interaction=0 || _initial_user_interaction=1

    prompt::disable_user_interaction >&2
    display::hide_cursor >&2

    local _message
    [[ -z "$*" ]] && _message='Your input' || _message="$*"
    _message="${_message}: "
    _prompt::display_message "${_message}" >&2

    _prompt::read_input >&2
    local _input="${_PROMPT__CURRENT_INPUT}"

    while [[ -z "${_input}" ]]; do
        _prompt::reject_input '<empty>' >&2
        _prompt::read_input >&2
        _input="$(text::trim_whitespace "${_PROMPT__CURRENT_INPUT}")"
    done
    _prompt::accept_input "$(text::sanitize "${_input}")" >&2

    echo -n "${_input}"

    _prompt::update_cursor_visibility >&2
    [[ "${_initial_user_interaction}" -eq 0 ]] && prompt::enable_user_interaction >&2
}

# Usage:  prompt::request_confirmation_yN [<message>]
#
# Requests user confirmation at the command prompt for a following operation.
# Valid inputs are y/n/yes/no, case-insesitive.
# ENTER defaults to 'no'.
#
#   message       a message describing the request to be confirmed
#   return value  true if user chose 'yes'; false for 'no'
function prompt::request_confirmation_yN() {
    trap '_prompt::resize' WINCH

    local _initial_user_interaction
    prompt::user_interaction_is_enabled && _initial_user_interaction=0 || _initial_user_interaction=1

    prompt::disable_user_interaction
    display::hide_cursor

    local _message
    [[ -z "$*" ]] && _message='Are you sure?' || _message="$*"
    _message="${_message} [y/N] "
    _prompt::display_message "${_message}"

    local _exit_status
    while true; do
        _prompt::read_input
        local _input="${_PROMPT__CURRENT_INPUT}"

        case "${_input:-n}" in
            [yY][eE][sS]|[yY])
                _prompt::accept_input 'yes'
                _exit_status=0
                break
                ;;
            [nN][oO]|[nN])
                _prompt::accept_input 'no'
                _exit_status=1
                break
                ;;
            *)
                _prompt::reject_input "$(text::sanitize "${_input}")"
                ;;
        esac
    done

    _prompt::update_cursor_visibility
    [[ "${_initial_user_interaction}" -eq 0 ]] && prompt::enable_user_interaction

    return ${_exit_status}
}

# Usage:  prompt::request_confirmation_Yn [<message>]
#
# Requests user confirmation at the command prompt for a following operation.
# Valid inputs are y/n/yes/no, case-insesitive.
# ENTER defaults to 'yes'.
#
#   message       a message describing the request to be confirmed
#   return value  true if user chose 'yes'; false for 'no'
function prompt::request_confirmation_Yn() {
    trap '_prompt::resize' WINCH

    local _initial_user_interaction
    prompt::user_interaction_is_enabled && _initial_user_interaction=0 || _initial_user_interaction=1

    prompt::disable_user_interaction
    display::hide_cursor

    local _message
    [[ -z "$*" ]] && _message='Are you sure?' || _message="$*"
    _message="${_message} [Y/n] "
    _prompt::display_message "${_message}"

    local _exit_status
    while true; do
        _prompt::read_input
        local _input="${_PROMPT__CURRENT_INPUT}"

        case "${_input:-y}" in
            [yY][eE][sS]|[yY])
                _prompt::accept_input 'yes'
                _exit_status=0
                break
                ;;
            [nN][oO]|[nN])
                _prompt::accept_input 'no'
                _exit_status=1
                break
                ;;
            *)
                _prompt::reject_input "$(text::sanitize "${_input}")"
                ;;
        esac
    done

    _prompt::update_cursor_visibility
    [[ "${_initial_user_interaction}" -eq 0 ]] && prompt::enable_user_interaction

    return ${_exit_status}
}

# Usage: prompt::choice <option_1> [<option_i>...]
#
# Prompts the user to choose one of the given options.
#
# By default, options can be selected by their number.
#
# Additionally, case-insensitive shortcuts consisting of one letter (or even
# a sequence of letters) may be encoded into an option using square brackets, 
# e.g. "do [t]his", "do t[hat]"
# The chosen shortcut must be unique in the options array.
#
# Additional constraints apply for the content of the options. See documentation 
# of function display::as_columns for details.
#
#   <option_1>, ..., <option_i>   an array of options; double-quote items that 
#                                 contain spaces to prevent them from being split
#   return value                  the option chosen by the user
function prompt::choice() {
    trap '_prompt::resize' WINCH

    local _initial_user_interaction
    prompt::user_interaction_is_enabled && _initial_user_interaction=0 || _initial_user_interaction=1

    prompt::disable_user_interaction >&2
    display::hide_cursor >&2

    util::mandatory_min_arg_count 1 "$@"
    local readonly _options=("$@")
    local readonly _size="${#_options[@]}"

    # apply shortcut style on shortcuts in options, for display only
    local _options_to_display=()
    local _option
    for _option in "${_options[@]}"; do
        _options_to_display+=("$(_prompt::format_shortcuts "${_option}")")
    done

    display::as_columns "${_options_to_display[@]}" >&2
    echo >&2
    _prompt::display_message '#? ' >&2

    local _choice=''
    while true; do
        _prompt::read_input >&2
        local _choice="${_PROMPT__CURRENT_INPUT}"

        # if the user typed a number, check the range
        if  [[ "${_choice}" =~ ^[1-9][0-9]*$ ]]; then
            if [[ "${_choice}" -gt 0 && "${_choice}" -le "${_size}" ]]; then
                break
            fi
        
        # if the user types a single letter, try to map a possible shortcut
        elif [[ "${_choice}" =~ ^[a-zA-Z]+$ ]]; then
            local _choice_lowercase=$(text::lower "${_choice}")

            local _shortcut_choice=''
            local i
            for (( i=0; i<_size; i++ )); do
                if [[ "$(text::lower "${_options[$i]}")" =~ ^.*\[${_choice_lowercase}\].*$ ]]; then
                    _shortcut_choice=$((i+1))
                    break
                fi
            done
            if [[ -n "${_shortcut_choice}" ]]; then
                _choice="${_shortcut_choice}"
                break;
            fi
        fi

        if [[ -n "${_choice}" ]]; then
            _prompt::reject_input "$(text::sanitize "${_choice}")" >&2
        else
            _prompt::reject_input '<empty>' >&2
        fi
    done

    local readonly _chosen_option="${_options[((_choice-1))]}"

    # format the chosen option for its display as accepted input
    local readonly _old_PROMPT__SHORTCUT_ATTRS=("${PROMPT__SHORTCUT_ATTRS[@]}")
    local readonly _old_PROMPT__POST_SHORTCUT_ATTRS=("${PROMPT__POST_SHORTCUT_ATTRS[@]}")

    PROMPT__SHORTCUT_ATTRS=("${PROMPT__VALID_INPUT_ATTRS[@]}")
    PROMPT__POST_SHORTCUT_ATTRS=("${PROMPT__VALID_INPUT_ATTRS[@]}")

    _prompt::accept_input "$(_prompt::format_shortcuts "${_chosen_option}")" >&2

    PROMPT__SHORTCUT_ATTRS=("${_old_PROMPT__SHORTCUT_ATTRS[@]}")
    PROMPT__POST_SHORTCUT_ATTRS=("${_old_PROMPT__POST_SHORTCUT_ATTRS[@]}")

    echo "${_chosen_option}"

    _prompt::update_cursor_visibility >&2
    [[ "${_initial_user_interaction}" -eq 0 ]] && prompt::enable_user_interaction >&2
}

# Usage: prompt::disable_user_interaction
#
# The user will not be able to interact with the prompt until 
# prompt::enable_user_interaction is called.
function prompt::disable_user_interaction() {
    stty -echo > /dev/null 2>&1
}

# Usage: prompt::enable_user_interaction
#
# The user is again able to interact with the prompt.
function prompt::enable_user_interaction() {
    _prompt::flush_stdin
    stty echo > /dev/null 2>&1
}

# Usage: prompt::user_interaction_is_enabled
#
#   return value    0, if the user can interact with the prompt; 1, otherwise
function prompt::user_interaction_is_enabled() {
    stty --all | grep --quiet -w '\-echo' && return 1 || return 0
}


### private constants and functions ###

function _prompt::resize() {
    if [[ "${_PROMPT__READING_INPUT}" = 'true' ]]; then
        _prompt::erase_text_and_redisplay_prompt "${_PROMPT__CURRENT_INPUT}${_PROMPT__CURSOR}"
        _PROMPT__SCREEN_WIDTH=$(display::get_screen_width)

        format::set "${PROMPT__USER_INPUT_ATTRS[@]}"
        echo -n "${_PROMPT__CURRENT_INPUT}"
        format::set "${PROMPT__POST_USER_INPUT_ATTRS[@]}"

        _prompt::display_cursor

        local readonly _full_text="${_PROMPT__CURRENT_MESSAGE}${_PROMPT__CURRENT_INPUT}"
        local readonly _full_text_size="${#_full_text}"
        (( _PROMPT__POSITION_ON_CURRENT_LINE = _full_text_size % _PROMPT__SCREEN_WIDTH ))
    fi
}

function _prompt::flush_stdin() {
    local _discard_me
    # shellcheck disable=SC2034
    while read -t 0.01 -n 100 _discard_me; do
        true
    done
}

function _prompt::update_cursor_visibility() {
    if [[ "${PROMPT__KEEP_CURSOR_HIDDEN}" = 'true' ]]; then
        display::hide_cursor
    else
        display::show_cursor
    fi
}

_PROMPT__CURRENT_MESSAGE=''
function _prompt::display_message() {
    util::mandatory_arg_count 1 "$@"
    _PROMPT__CURRENT_MESSAGE="$1"
    echo -n "${_PROMPT__CURRENT_MESSAGE}"
}

_PROMPT__CURRENT_INPUT=''
_PROMPT__POSITION_ON_CURRENT_LINE=0
_PROMPT__SCREEN_WIDTH=0
_PROMPT__READING_INPUT='false'
function _prompt::read_input() {
    _PROMPT__CURRENT_INPUT=''
    _PROMPT__POSITION_ON_CURRENT_LINE=${#_PROMPT__CURRENT_MESSAGE}
    _PROMPT__SCREEN_WIDTH=$(display::get_screen_width)

    _prompt::display_cursor
    _prompt::flush_stdin

    _PROMPT__READING_INPUT='true'

    # we can't use "while IFS= read ..." because if the resize trap is triggered, 
    # it resets the IFS to its default value for the current read 
    # => a TAB or SPACE immediately after a resize will terminate the read
    local _old_IFS="${IFS}"
    IFS=
 
    # we read char by char in order to fully control how the user input is 
    # displayed, i.e. untabification, sanitization
    local _char
    while read -r -n 1 _char; do
        if [[ "${_char}" = $(echo -ne "${CODE_LF}") ]]; then
            break
        elif [[ "${_char}" = $(echo -ne "${CODE_DEL}") || "${_char}" = $(echo -ne "${CODE_BS}") ]]; then
            # the Backspace key generates CODE_DEL; Ctrl+H generates CODE_BS
            if [[ -n "${_PROMPT__CURRENT_INPUT}" ]]; then
                _prompt::erase_last_character
                _PROMPT__CURRENT_INPUT="${_PROMPT__CURRENT_INPUT:0:-1}"
            fi
        else
            _char="$(text::sanitize_and_untabify "${_char}")"
            _prompt::display_input "${_char}"
            _PROMPT__CURRENT_INPUT="${_PROMPT__CURRENT_INPUT}${_char}"
        fi
    done

    _PROMPT__READING_INPUT='false'

    IFS="${_old_IFS}"
}

# The cursor must be a single character! All cursor-movement computations are 
# based on this assumption.
readonly _PROMPT__CURSOR='_' 
function _prompt::display_cursor() {
    format::set "${COLOUR_GREEN}"
    echo -n "${_PROMPT__CURSOR}"
    format::set "${COLOUR_DEFAULT}"
}

function _prompt::erase_last_character() {
    if (( _PROMPT__POSITION_ON_CURRENT_LINE == 0 )); then
        # erase our cursor
        tput cub 1 2> /dev/null
        tput el 2> /dev/null

        # erase the last character on the line above
        tput cuu 1 2> /dev/null
        tput cuf "${_PROMPT__SCREEN_WIDTH}" 2> /dev/null
        tput el 2> /dev/null

        (( _PROMPT__POSITION_ON_CURRENT_LINE = _PROMPT__SCREEN_WIDTH - 1 ))

    elif (( _PROMPT__POSITION_ON_CURRENT_LINE == _PROMPT__SCREEN_WIDTH - 1 )); then
        # we are positioned on the cursor => move back one character
        tput cub 1 2> /dev/null
        tput el 2> /dev/null

        (( _PROMPT__POSITION_ON_CURRENT_LINE-- ))

    else
        # we are positioned in front of the cursor => move back 2 characters
        tput cub 2 2> /dev/null
        tput el 2> /dev/null

        (( _PROMPT__POSITION_ON_CURRENT_LINE-- ))
    fi

    _prompt::display_cursor
}

function _prompt::display_input() {
    util::mandatory_arg_count 1 "$@"
    local readonly _input="$1"
    local readonly _input_size=${#_input}

    _prompt::erase_cursor

    # compute new position
    (( _PROMPT__POSITION_ON_CURRENT_LINE += _input_size ))
    (( _PROMPT__POSITION_ON_CURRENT_LINE = _PROMPT__POSITION_ON_CURRENT_LINE % _PROMPT__SCREEN_WIDTH ))

    # display input and cursor
    format::set "${PROMPT__USER_INPUT_ATTRS[@]}"
    echo -n "${_input}"
    format::set "${PROMPT__POST_USER_INPUT_ATTRS[@]}"

    _prompt::display_cursor
}

function _prompt::erase_cursor() {
    if (( _PROMPT__POSITION_ON_CURRENT_LINE  == (_PROMPT__SCREEN_WIDTH - 1) )); then
        # move to the last position on the current line
        tput cuf _PROMPT__SCREEN_WIDTH 2> /dev/null
        true
    else
        # move back one character
        tput cub 1 2> /dev/null
    fi

    # erase until end of line
    tput el 2> /dev/null
}

function _prompt::accept_input() {
    util::mandatory_arg_count 1 "$@"
    local readonly _accepted_input="$1"

   _prompt::erase_text_and_redisplay_prompt "${_PROMPT__CURRENT_INPUT}${_PROMPT__CURSOR}"

    format::set "${PROMPT__VALID_INPUT_ATTRS[@]}"
    echo -n "${_accepted_input}"
    format::set "${PROMPT__POST_VALID_INPUT_ATTRS[@]}"

    echo
}

function _prompt::reject_input() {
    util::mandatory_arg_count 1 "$@"
    local readonly _rejected_input="$1"

    _prompt::erase_text_and_redisplay_prompt "${_PROMPT__CURRENT_INPUT}${_PROMPT__CURSOR}"

    format::set "${PROMPT__INVALID_INPUT_ATTRS[@]}"
    echo -n "${_rejected_input}"
    format::set "${PROMPT__POST_INVALID_INPUT_ATTRS[@]}"

    sleep 1
    
    _prompt::erase_text_and_redisplay_prompt "${_rejected_input}"
}

function _prompt::erase_text_and_redisplay_prompt() {
    util::mandatory_arg_count 1 "$@"
    local readonly _text="$1"

    local readonly _text_with_prompt="${_PROMPT__CURRENT_MESSAGE}${_text}"
    local readonly _total_length=${#_text_with_prompt}
    local readonly _screen_width=${_PROMPT__SCREEN_WIDTH}

    local _lines_of_input
    (( _lines_of_input=_total_length / _screen_width ))
    if (( _total_length % _screen_width != 0 )); then
        (( _lines_of_input++ ))
    fi

    while (( _lines_of_input > 0 )); do
        tput cuu 1 2> /dev/null
        (( _lines_of_input-- ))
    done
    echo
    tput ed 2> /dev/null

    echo -n "${_PROMPT__CURRENT_MESSAGE}"
}

function _prompt::format_shortcuts() {
    util::mandatory_arg_count 1 "$@"
    local readonly _option="$1"
    echo "${_option}" | sed -r "s/\\[([a-zA-Z]+)\\]/$(format::set "${PROMPT__SHORTCUT_ATTRS[@]}")\\1$(format::set "${PROMPT__POST_SHORTCUT_ATTRS[@]}")/g"
}

