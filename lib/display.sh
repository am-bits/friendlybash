# Copyright 2016 Andreea Mocanu & Alexandru Mocanu
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ----------------------------------------------------------------------
# Authors:
#   andreea.oana.mocanu@gmail.com
#   alexandru.mocanu@gmail.com
#
# Sources:
#   https://bitbucket.org/am-bits/friendlybash

[[ -z "${_display_sh_included}" ]] && _display_sh_included='yes' || return

source "${FRIENDLY_BASH_ROOT}/lib/text.sh"
source "${FRIENDLY_BASH_ROOT}/lib/util.sh"
source "${FRIENDLY_BASH_ROOT}/lib/format.sh"
source "${FRIENDLY_BASH_ROOT}/lib/stringutil.sh"

readonly FRIENDLYBASH_DISPLAY__COLUMN_LABEL_FORMAT=' xxx) '

# Usage: display::trim_to_screen <text> [<terminator>]
#
# Returns a one-line substring of the given text, terminated with the given terminator,
# that fits precisely inside the current screen.
#
# If no terminator is given, the default one ('...') is used.
#
# If the text has multiple lines, only the first one is considered.
# TABs are displayed using the value of TEXT__SPACES_PER_TAB.
#
# Do NOT trim text containing colours or other format attributes; trimming sanitizes the text!
# If necessary, apply the colours after trimming.
#
#   text          the text to trim
#   terminator    a terminator to mark the trimming
#   return value  the trimmed text
function display::trim_to_screen() {
    util::mandatory_min_arg_count 1 "$@"

    local _string=$(text::sanitize_and_untabify "$1")
    local _is_multiline
    if [[ $(echo -n "$1" | wc -l) -eq 0 ]]; then
        _is_multiline=1
    else
        _is_multiline=0
        # if multi-line string, keep only the first line
        _string=$(echo -n "${_string}" | cut -d $'\n' -f 1)
    fi

    # default terminator, unless another is given, is '...'
    if [[ "$#" -eq 2 ]]; then
        local readonly _terminator="$2"
    else
        local readonly _terminator='...'
    fi

    local readonly _string_width="${#_string}"

    # shellcheck disable=SC2119
    local readonly _screen_width=$(display::get_screen_width)

    if [[ ${_string_width} -le ${_screen_width} && "${_is_multiline}" -ne 0 ]]; then
        echo "${_string}"
    else
        local readonly _terminator_width="${#_terminator}"
        local _new_width
        (( _new_width=_screen_width-_terminator_width ))

        echo "${_string:0:${_new_width}}${_terminator}"
    fi
}

# Usage: display::as_colums <item_1> [<item_i>...]
# 
# Displays the given items as numbered columns, counting vertically.
#
# The width of each column is equal to the width of the widest item. 
# Format sequences (e.g. [4;31m ) do NOT count towards the width, but 
# other non-printable characters do.
# 
# If some items are wider than the screen, or if they contain multiple lines, 
# they will NOT be trimmed! Use the function display::trim_column for this.
#
# If the items contain non-printable characters (other than format sequences)
# the columns might not be displayed correctly.
#
#   item_1, ..., item_i    an array of items; double-quote items that contain 
#                          spaces to prevent them from being split
function display::as_columns() {
    util::mandatory_min_arg_count 1 "$@"
    local readonly _items=("$@")
    local readonly _size="${#_items[@]}"
    
    local readonly _label_format="${FRIENDLYBASH_DISPLAY__COLUMN_LABEL_FORMAT}"
    local readonly _label_width="${#_label_format}"

    # the texts to be displayed for each item
    local _texts=()

    # the visible widths of the texts, i.e. without taking into account color sequences
    local _visible_widths=()

    # the width of a column will be the width of largest item in the array
    local _width=0

    # 1st pass through array: compute width; cache texts and visible widths
    local i
    for ((i=0; i<_size; i++)); do 
        local _item="${_items[$i]}"
        _item=$(text::untabify "${_item}")

        local _label="$((i + 1))) "
        _label="$(printf "%+${_label_width}s" "${_label}")"

        local _text="${_label}${_item}"
        _texts+=("${_text}")

        local _visible_text="${_text}"
        _visible_text=$(text::remove_format_sequences "${_visible_text}")

        local _visible_width="${#_visible_text}"
        _visible_widths+=("${_visible_width}")

        (( _visible_width > _width )) && _width=${_visible_width}
    done
    declare -r _width

    # compute the maximum number of columns that fit inside the screen
    # shellcheck disable=SC2119
    local readonly _screen_width=$(display::get_screen_width)
    local _cols=$(( _screen_width / _width ))
    (( _cols == 0 )) && _cols=1
    declare -r _cols

    # compute the number of rows, based on number of columns
    local _rows=$(( _size / _cols ))
    (( _size % _cols == 0 )) || (( _rows++ ))
    declare -r _rows

    # 2nd pass through array: build and display the menu line by line
    local _buffer=''
    local _col=0
    local _row=0
    local i
    for ((i=0; i<_size; i++)); do 
        local j=$((_row + _rows * _col))

        # end of row
        if (( j >= _size )); then
            _col=0
            (( _row++ ))
            (( j=_row + _rows*_col ))
            echo "${_buffer}"
            _buffer=''
        fi

        (( _col++ ))

       _buffer="${_buffer}${_texts[j]}"

        # add padding up to the maximum column width
        local _padding_width=$(( _width - _visible_widths[j] ))
        if (( _padding_width > 0 )); then
            _buffer="${_buffer}$(printf "%${_padding_width}s" " ")"
        fi
    done

    echo "${_buffer}"
}

# Usage: display::trim_column <item> [<terminator>]
#
# This function should be used on each of the items passed to display::as_columns, 
# if you want to prevent items wider than the screen from being displayed on more 
# than one line.
#
# Do NOT trim columns containing colours or other format attributes; trimming sanitizes the text!
# If necessary, apply the colours after trimming.
#
#   item            a string to be displayed on a column
#   terminator      an optional trim terminator
#   return value    the item, trimmed to fit inside the screen
function display::trim_column() {
    util::mandatory_min_arg_count 1 "$@"
    local _item="$1"
    local readonly _terminator="$2"

    local readonly _label="${FRIENDLYBASH_DISPLAY__COLUMN_LABEL_FORMAT}"

    if [[ $# -eq 1 ]]; then
        _item=$(display::trim_to_screen "${_label}${_item}")
    else
        _item=$(display::trim_to_screen "${_label}${_item}" "${_terminator}")
    fi
    _item="${_item:${#_label}}"

    echo -n "${_item}"
}

# Usage: display::horizontal_band [<colour>]
#
# Displays a horizontal band across the whole width of the screen, in the given
# colour. If no colour is given, the default current foreground colour is used.
#
#   colour    the colour of the band
function display::horizontal_band() {
    local readonly _colour="$1"
    if [[ -n "${_colour}" ]]; then
        format::set "${_colour}"
    fi
    format::set "${STYLE_INVERTED}"

    # shellcheck disable=SC2119
    local readonly _screen_width=$(display::get_screen_width)
    printf "%${_screen_width}s\n" " "

    format::set "${STYLE_NOT_INVERTED}"
    echo
}

# Usage: display::horizontal_line [<colour>]
#
# Displays a horizontal line across the whole width of the screen, in the given
# colour. If no colour is given, the current foreground colour is used.
#
#   colour    the colour of the line
function display::horizontal_line() {
    local readonly _colour="$1"
    if [[ -n "${_colour}" ]]; then
        format::set "${_colour}"
    fi

    # shellcheck disable=SC2119
    local readonly _screen_width=$(display::get_screen_width)
    stringutil::repeat '-' "${_screen_width}"
    echo
}

# Usage: display::go_to_alternate_screen
#
# Saves the current cursor position and moves to the alternate screen buffer.
function display::go_to_alternate_screen() {
    util::recommended_arg_count 0 "$@"
    tput smcup 2> /dev/null
}

# Usage: display::go_to_normal_screen
#
# Clears the screen buffer (assumed alternate), moves to the normal screen buffer and restores the cursor position.
function display::go_to_normal_screen() {
    util::recommended_arg_count 0 "$@"
    tput rmcup 2> /dev/null
}

# Usage: display::hide_cursor
#
# Hides the cursor from the screen. Can be restored using 'display::show_cursor'.
function display::hide_cursor() {
    util::recommended_arg_count 0 "$@"
    tput civis 2> /dev/null
}

# Usage: display::show_cursor
#
# Restores the cursor on the screen, after being hiden previously using 'display::hide_cursor'.
function display::show_cursor() {
    util::recommended_arg_count 0 "$@"
    tput cvvis 2> /dev/null
}

# Usage: display::get_screen_width
#
# Returns the number of columns in the terminal window, or the default value (80) if screen width cannot be determined.
#
# IMPORTANT! This function will not work as expected in pipes, e.g.
#            echo "${long_string}" | cut -c 1-$(display::get_screen_width)
#
#            In this case, use a variable instead of calling the function directly.
#
# shellcheck disable=SC2120
function display::get_screen_width() {
    util::recommended_arg_count 0 "$@"

    stty size > /dev/null 2>&1
    if [[ $? -eq 0 ]]; then
        stty size | cut -d ' ' -f 2
    else
        echo '80'
    fi
}

# Usage: display::get_screen_height
#
# Returns the number of lines in the terminal window, or the default value (24) if screen height cannot be determined.
#
# IMPORTANT! This function will not work as expected in pipes. 
#            (See more about this in the display::get_screen_width documentation.)
#
# shellcheck disable=SC2120
function display::get_screen_height() {
    util::recommended_arg_count 0 "$@"

    stty size > /dev/null 2>&1
    if [[ $? -eq 0 ]]; then
        stty size | cut -d ' ' -f 1
    else
        echo '24'
    fi
}

