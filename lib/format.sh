# Copyright 2016 Andreea Mocanu & Alexandru Mocanu
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ----------------------------------------------------------------------
# Authors:
#   andreea.oana.mocanu@gmail.com
#   alexandru.mocanu@gmail.com
#
# Sources:
#   https://bitbucket.org/am-bits/friendlybash

[[ -z "${_format_sh_included}" ]] && _format_sh_included='yes' || return

source "${FRIENDLY_BASH_ROOT}/lib/identify.sh"
source "${FRIENDLY_BASH_ROOT}/lib/util.sh"

# Usage:  format::set <attibute_1> [<attribute_i>...]
#
# Changes the text style, colour or background according to the given attribute(s).
#
# attribute_i   an attribute; use the pre-defined attribute constants in this library
function format::set() {
    util::mandatory_min_arg_count 1 "$@"

    local _attr=''
    local i=0
    local n
    for n in "$@"; do
        (( i++ ))
        if [[ "$n" =~ ^[0-9][0-9]*$ ]]; then
            if [[ -z "${_attr}" ]]; then
                _attr="$n"
            else
                _attr="${_attr};$n"
            fi
        else
            util::fatal_error "$(identify::call): $(identify::this_function) expected a number as \$$i, received '$n'"
        fi
    done

    echo -n "[${_attr}m"
}

# Usage:  format::default
#
# Removes any previously set attributes (style, color and background).
function format::default() {
    util::recommended_arg_count 0 "$@"
    format::set "${STYLE_NONE}"
}


# style constants
readonly STYLE_NONE=0

readonly STYLE_BOLD=1
readonly STYLE_DIM=2
readonly STYLE_UNDERLINED=4
readonly STYLE_BLINK=5
readonly STYLE_INVERTED=7
readonly STYLE_HIDDEN=8

readonly STYLE_NOT_BOLD=21
readonly STYLE_NOT_DIM=22
readonly STYLE_NOT_UNDERLINED=24
readonly STYLE_NOT_BLINK=25
readonly STYLE_NOT_INVERTED=27
readonly STYLE_NOT_HIDDEN=28


# foreground colour constants
readonly COLOUR_DEFAULT=39
readonly COLOUR_BLACK=30
readonly COLOUR_RED=31
readonly COLOUR_GREEN=32
readonly COLOUR_YELLOW=33
readonly COLOUR_BLUE=34
readonly COLOUR_MAGENTA=35
readonly COLOUR_CYAN=36
readonly COLOUR_LIGHT_GRAY=37
readonly COLOUR_DARK_GRAY=90
readonly COLOUR_LIGHT_RED=91
readonly COLOUR_LIGHT_GREEN=92
readonly COLOUR_LIGHT_YELLOW=93
readonly COLOUR_LIGHT_BLUE=94
readonly COLOUR_LIGHT_MAGENTA=95
readonly COLOUR_LIGHT_CYAN=96
readonly COLOUR_WHITE=97

# background colour constants
readonly BACKGROUND_DEFAULT=49
readonly BACKGROUND_BLACK=40
readonly BACKGROUND_RED=41
readonly BACKGROUND_GREEN=42
readonly BACKGROUND_YELLOW=43
readonly BACKGROUND_BLUE=44
readonly BACKGROUND_MAGENTA=45
readonly BACKGROUND_CYAN=46
readonly BACKGROUND_LIGHT_GRAY=47
readonly BACKGROUND_DARK_GRAY=100
readonly BACKGROUND_LIGHT_RED=101
readonly BACKGROUND_LIGHT_GREEN=102
readonly BACKGROUND_LIGHT_YELLOW=103
readonly BACKGROUND_LIGHT_BLUE=104
readonly BACKGROUND_LIGHT_MAGENTA=105
readonly BACKGROUND_LIGHT_CYAN=106
readonly BACKGROUND_WHITE=107

