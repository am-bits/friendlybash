# Copyright 2016 Andreea Mocanu & Alexandru Mocanu
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ----------------------------------------------------------------------
# Authors:
#   andreea.oana.mocanu@gmail.com
#   alexandru.mocanu@gmail.com
#
# Sources:
#   https://bitbucket.org/am-bits/friendlybash

[[ -z "${_friendlybash_sh_included}" ]] && _friendlybash_sh_included='yes' || return

source "${FRIENDLY_BASH_ROOT}/lib/identify.sh"
source "${FRIENDLY_BASH_ROOT}/lib/log.sh"
source "${FRIENDLY_BASH_ROOT}/lib/util.sh"
source "${FRIENDLY_BASH_ROOT}/lib/network.sh"
source "${FRIENDLY_BASH_ROOT}/lib/text.sh"
source "${FRIENDLY_BASH_ROOT}/lib/format.sh"
source "${FRIENDLY_BASH_ROOT}/lib/display.sh"
source "${FRIENDLY_BASH_ROOT}/lib/prompt.sh"

