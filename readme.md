# FriendlyBash #

FriendlyBash is a collection of [Bash](https://www.gnu.org/software/bash/) functions that aim to make your life easier when writing shell scripts.

## How to install ##

You can install the latest FriendlyBash version either by using an interactive install script or manually.

You can install older FriendlyBash versions manually. However, it is strongly recommended to always use the latest version.

### Interactive install script ###

1. Download the [install script](https://bitbucket.org/am-bits/friendlybash/downloads/install_friendly_bash).

1. Run the script and follow the instructions on screen.

1. You may remove the script when the installation is complete.

You can also use this script to update an existing installation of FriendlyBash to the latest version, or to re-install it at a different location.

The script is safe to run at any time. It first lets you know what version you have installed (if any) and what is the latest version. After reviewing this information, you will be able to choose whether to (re-)install the latest version or not.

### Manual installation ###
1. Download the [latest version](https://bitbucket.org/am-bits/friendlybash/downloads/friendlybash_latest.tar) of FriendlyBash. Older versions can be found [here](https://bitbucket.org/am-bits/friendlybash/downloads).

1. Extract the contents of the tar archive at a chosen location on your machine, e.g.

        tar -xvf friendlybash_v1.0.tar -C /my/path/to/friendlybash

1. Add the following definitions to your `/etc/environment` file:

        FRIENDLY_BASH_ROOT=/my/path/to/friendlybash
        FRIENDLY_BASH=/my/path/to/friendlybash/friendlybash.sh

## How to use ##

1. Import the FriendlyBash libraries in your script:

		#!/bin/bash
		source "${FRIENDLY_BASH}"

1. You can now call any FriendlyBash function, e.g.

		log::info 'Please enjoy FriendlyBash!'

Run this `grep` search to see all available FriendlyBash functions:

	grep -nor "function [a-zA-Z][a-zA-Z0-9_]*::[a-zA-Z0-9_]\+()" ${FRIENDLY_BASH_ROOT}/lib/

The documentation for each function can be found together with the function definition in the respective `${FRIENDLY_BASH_ROOT}/lib/<library>.sh` file.

## Dependencies ##
FriendlyBash was developed and tested with **GNU Bash 4.3** on **Ubuntu 15.04**.

To a lesser extent, it was also tested on **Windows 7 Professional SP1 64-bit** (via Git Bash/mintty 2.0.3).

FriendlyBash uses:

* **GNU coreutils 8.23** (`ls`, `tr`, `cut`, `du`, `stty`)
* **GNU grep 2.20**
* **GNU Wget 1.16.1**
* **acpi 1.7**
* **iputils-121221** (`ping`)
* **ncurses 5.9.20140712** (`tput`)

## Development ##

### Setup ###

1. Clone the repository. (Duh!)

1. Add the following definitions to your `/etc/environment` file:

        FRIENDLY_BASH_ROOT=/my/friendlybash/repository
        FRIENDLY_BASH=/my/friendlybash/repository/friendlybash.sh

1. Install [ShellCheck](https://github.com/koalaman/shellcheck).

### Development guidelines ###

1. "Public" functions in a *library.sh* file are prefixed with `library::` and "public" variables/constants are prefixed with `LIBRARY__`, e.g. *prompt.sh* => `prompt::input()`, `PROMPT__KEEP_CURSOR_HIDDEN`

1. "Private" functions in a *library.sh* file are prefixed with `_library::` and "private" variables/constants are prefixed with `_LIBRARY__`, e.g. _*prompt.sh* => `_prompt::read_input()`, `_PROMPT__POSITION_ON_CURRENT_LINE`

1. Use an include guard at the beginning of each library file, e.g. for *prompt.sh*:

		[[ -z "${_prompt_sh_included}" ]] && _prompt_sh_included='yes' || return

1. All "public" functions must have their usage documented in a comment before their definition.
    
1. Do not commit code with ShellCheck warnings! If you have a good reason for not solving the warning, disable it with `# shellcheck disable=SCxxxx`.